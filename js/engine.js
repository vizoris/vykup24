
$(function() {

// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})


// Выпадающее меню
if ($(window).width() >= 992) {
   $('.dropdown').hover(function() {
    $(this).children('.dropdown-menu').stop(true,true).fadeToggle();
   })
}else {
  $('.dropdown').click(function() {
    $(this).children('.dropdown-menu').stop(true,true).fadeToggle();
   })
}


// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.header').toggleClass("active");
  $('.main-menu').fadeToggle(200);
});


// FAQ
$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});


// FansyBox
 $('.fancybox').fancybox({});




// Сллайдер авто
$('.auto-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});



// Прикрепляем файлы

$('.attach').each(function() { // на случай, если таких групп файлов будет больше одной
  var attach = $(this),
    fieldClass = 'attach__item', // класс поля
    attachedClass = 'attach__item--attached', // класс поля с файлом
    fields = attach.find('.' + fieldClass).length, // начальное кол-во полей
    fieldsAttached = 0; // начальное кол-во полей с файлами

  var newItem = '<div class="attach__item"><label><div class="attach__up">Добавить файл</div><input class="attach__input" type="file" name="files[]" /></label><div class="attach__delete"><i class="sprite sprite-cross"></i></div><div class="attach__name"></div><div class="attach__edit">Изменить</div></div>'; // разметка нового поля

  // При изменении инпута
  attach.on('change', '.attach__input', function(e) {
    var item = $(this).closest('.' + fieldClass),
      fileName = '';
    if (e.target.value) { // если value инпута не пустое
      fileName = e.target.value.split('\\').pop(); // оставляем только имя файла и записываем в переменную
    }
    if (fileName) { // если имя файла не пустое
      item.find('.attach__name').text(fileName); // подставляем в поле имя файла
      if (!item.hasClass(attachedClass)) { // если в поле до этого не было файла
        item.addClass(attachedClass); // отмечаем поле классом
        fieldsAttached++;
      }
      if (fields < 10 && fields == fieldsAttached) { // если полей меньше 10 и кол-во полей равно
        item.after($(newItem)); // добавляем новое поле
        fields++;
      }
    } else { // если имя файла пустое
      if (fields == fieldsAttached + 1) {
        item.remove(); // удаляем поле
        fields--;
      } else {
        item.replaceWith($(newItem)); // заменяем поле на "чистое"
      }
      fieldsAttached--;

      if (fields == 1) { // если поле осталось одно
        attach.find('.attach__up').text('Загрузить файл'); // меняем текст
      }
    }
  });

  // При нажатии на "Изменить"
  attach.on('click', '.attach__edit', function() {
    $(this).closest('.attach__item').find('.attach__input').trigger('click'); // имитируем клик на инпут
  });

  // При нажатии на "Удалить"
  attach.on('click', '.attach__delete', function() {
    var item = $(this).closest('.' + fieldClass);
    if (fields > fieldsAttached) { // если полей больше, чем загруженных файлов
      item.remove(); // удаляем поле
      fields--;
    } else { // если равно
      item.after($(newItem)); // добавляем новое поле
      item.remove(); // удаляем старое
    }
    fieldsAttached--;
    if (fields == 1) { // если поле осталось одно
      attach.find('.attach__up').text('Загрузить файл'); // меняем текст
    }
  });
});


// Позиционирование блока на карте

var windowWidth = $(window).width();
var containerWidth = $('.container').width();
var leftPosition = (windowWidth-containerWidth)/2;
console.log(containerWidth);
$('.map-form').css('left',leftPosition +15 + 'px')



// Меню сайдбара
$('.sidebar-menu>li>a').click(function() {
  $(this).next('ul').fadeToggle()
})


// Меню моделей. Показать все модели
$('.models-list__more').click(function() {
  $(this).toggleClass('active');
  if ($(this).hasClass('active')) {
    $(this).text('Скрыть');
  }else {
    $(this).text('Показать все');
  }

  $('.models-list__hidden').fadeToggle();
})



})